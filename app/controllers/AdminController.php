<?php

class AdminController extends BaseController
{
	
	public function index()
	{
		$sec = Section::all();

		return View::make('admin.index')->with('sections', $sec);
	}
}