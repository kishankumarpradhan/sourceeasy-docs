<?php

class SectionsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$section = Section::all();
		return View::make('docs.index')->with('sections', $section);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('sections.CreateSection');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validate = Validator::make(Input::all(), array(
			'name' => 'required|unique:sections'
		));

		if($validate->fails())
		{
			return Redirect::to('sections/create')->withErrors($validate)->withInput();
		}
		else
		{
			$sec = new Section();
			$sec->name = Input::get('name');
			$sec->slug = str_replace(" ", "-", Input::get('name'));

			if ($sec->save())
			{
				return Redirect::to('admin')->with('success', 'Your section created successfully.');
			}
			else
			{
				return Redirect::to('sections/create')->with('fail', 'Section creation failed. Please try again.');
			}
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($slug)
	{
		$sec = Section::where('slug', '=', $slug)->first();
		if(count($sec) < 1)
		{
			return Redirect::to('sections')->with('fail', 'Sorry, No section found');
		}
		$ques = Question::where('section_id', '=', $sec->id)->get();
		return View::make('sections.index')
						->with('section_slugs', $slug)
						->with('questions', $ques);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($slug)
	{
		$sec = Section::where('slug', '=', $slug)->first();
		$ques = Question::where('section_id', '=', $sec->id)->get();
		return View::make('sections.EditSection')
							->with('section', $sec)
							->with('section_slug', $slug)
							->with('questions', $ques);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($slug)
	{
		$sec = Section::where('slug', '=', $slug)->first();
		$val = Input::get('rename');
		$sec->name = $val;
		$sec->slug = str_replace(" ", "-", $val);
		$slug = $sec->slug;
		if($sec->save())
		{
			return Redirect::to('sections/'.$slug.'/edit')->with('success', 'Updated');
		}
		else
		{
			return Redirect::to('sections/'.$slug.'/edit')->with('fail', 'not Updated');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($slug)
	{

		$validate = Validator::make(Input::all(), array(
			'sid' => 'required'
		));

		if($validate->fails())
		{
			return "Section cannot be null";
		}
		else
		{
			$sid = Input::get('sid');
			$sec = Section::where('id', '=', $sid)->where('slug', '=', $slug)->first();
			$ques = Question::where('section_id', '=', $sid)->delete();

			if(count($sec) < 1)
			{
				return "Section Not Found";
			}
			else
			{
				if($sec->delete())
				{	
					return "success";
				}
				else
				{
					return "failed";
				}
			}
		}
	}

}
