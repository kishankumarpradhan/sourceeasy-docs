<?php

class SectionsQuestionsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return "ques index";
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return "ques create";
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($slug)
	{
		$validate = Validator::make(Input::all(), array(
			'title' => 'required',
			'descriptions' => 'required'
		));

		if($validate->fails())
		{
			return Redirect::to('sections/'.$slug.'/edit')->withErrors($validate)->withInput();
		}
		else
		{
			$sec_id = Section::getId($slug);
			$ques = new Question();
			$ques->section_id = $sec_id;
			$ques->title = Input::get('title');
			$ques->descriptions = Input::get('descriptions');
			$ques->slug = str_replace(" ", "-", Input::get('title'));

			if($ques->save())
			{
				return Redirect::to('sections/'.$slug.'/edit')->with('EQsuccess', 'Questions created');
			}
			else
			{
				return Redirect::to('sections/'.$slug.'/edit')->with('EQfail', 'Questions creation failed');
			}
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($sec_slug, $ques_slug)
	{
		// $sec_id = Section::getId($sec_slug);
		// $ques_id = Question::getId($ques_slug);
		// $ques = Question::find($ques_id);
		$ques = Question::where('slug', '=', $ques_slug)->first();
		
		return View::make('sections.questions.index')->with('questions', $ques);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($slug,$qslug)
	{
		//$ques = Question::where('slug', '=', $qslug)->get();
		$ques_id = Question::getId($qslug);
		$ques = Question::find($ques_id);
		return View::make('sections.questions.EditQuestion')
								->with('section_slug', $slug)
								->with('questions', $ques);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($slug,$qslug)
	{
		$validate = Validator::make(Input::all(), array(
			'title' => 'required',
			'descriptions' => 'required'
		));

		if($validate->fails())
		{
			return Redirect::to('sections/'.$slug.'/questions/'.$qslug.'/edit')->withErrors($validate)->withInput();
		}
		else
		{
			$sec_id = Section::getId($slug);
			$ques = Question::where('section_id', '=', $sec_id)->where('slug', '=', $qslug)->first();
			$ques->title = Input::get('title');
			$ques->descriptions = Input::get('descriptions');
			$ques->slug = str_replace(" ", "-", Input::get('title'));

			if($ques->save())
			{
				return Redirect::to('sections/'.$slug.'/edit')->with('EQsuccess'.$ques->id, 'Updated');
			}
			else
			{
				return Redirect::to('sections/'.$slug.'/edit')->with('EQfail'.$ques->id, 'Updation failed');
			}
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($slug,$qslug)
	{
		$qid = Input::get('qid');
		$qcheck = Question::where('id', '=', $qid)->where('slug', '=', $qslug)->first();
		if(count($qcheck) < 1)
		{
			return "Invalid question!!";
		}
		else
		{
			if($qcheck->delete())
			{
				return "success";
			}
			else
			{
				return "failed";
			}
		}
	}


}
