<?php

class QuestionSeeder extends Seeder{

	public function run()
	{
		$faker = Faker\Factory::create();

		foreach (range(1,5) as $index) {
			Question::create([
				'section_id' => 3,
				'title' => $faker->sentence(3),
				'descriptions' => $faker->sentence(3),
				'slug' => str_replace(' ', '-', $faker->sentence(3))
				]);
		}
	}
}

?>