<?php

class SectionSeeder extends Seeder{

	public function run()
	{
		$faker = Faker\Factory::create();

		foreach (range(1,20) as $index) {
			Section::create([
				'name' => $faker->sentence(3),
				'slug' => str_replace(' ', '-', $faker->sentence(3))
				]);
		}
	}
}

?>