<?php

class Question extends Eloquent{

	public static function getId($slug)
	{
		$ques = Question::where('slug', '=', $slug)->first();
		if(count($ques) < 1)
		{
			return Redirect::to('sections')->with('fail', 'Sorry, No questions found');
		}
		else
		{
			return $ques->id;
		}
	}

}