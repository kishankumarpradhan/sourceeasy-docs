<?php

class Section extends Eloquent{


	public static function getId($slug)
	{
		$sec = Section::where('slug', '=', $slug)->first();
		if(count($sec) < 1)
		{
			return Redirect::to('sections')->with('fail', 'Sorry, No section found');
		}
		else
		{
			return $sec->id;
		}
	}

}