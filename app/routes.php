<?php

Route::get('/', function()
{
	return Redirect::to('sections');
});


Route::resource('sections','SectionsController');

Route::resource('sections.questions', 'SectionsQuestionsController');

Route::resource('admin', 'AdminController');
// Route::get('admin', function()
// {
// 	return View::make('sections.index')->with('isAdmin', true);
// });