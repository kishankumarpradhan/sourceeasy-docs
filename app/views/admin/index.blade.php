@extends('layouts.master')

@section('head')
	@parent

	<title>Sourceeasy Admin</title>

@stop
@section('content')

	<div><a href="{{ URL::to('sections/create') }}">Add new Section</a></div><br><br>
	@foreach($sections as $section)
		<div id="section{{$section->id}}">
			<span>{{ $section->name }}</span>&nbsp;&nbsp;&nbsp;&nbsp;
			<span><a href="{{ URL::to('sections/'.$section->slug.'/edit') }}">Edit Section</a></span>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<span val="{{$section->slug}}" id="{{$section->id}}" check="same"><a href="javascript:void(0)" class="sdelete">Delete</a></span>
			<hr>
		</div>
	@endforeach


@stop