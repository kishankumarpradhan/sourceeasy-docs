@extends('layouts.master')

@section('head')
	@parent

	<title>Sourceeasy Docs</title>

@stop
@section('content')

@foreach($sections as $section)
	<div><a href="{{ URL::to('sections/'.$section->slug) }}">{{ $section->name }}</a></div>
@endforeach

@stop