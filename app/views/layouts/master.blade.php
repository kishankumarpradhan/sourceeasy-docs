<!doctype html>
<html lang="en">
<head>
	@section('head')
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	{{ HTML::style('css/bootstrap.min.css') }}
	{{ HTML::script('js/jquery.js') }}
	{{ HTML::script('js/main.js') }}
	{{ HTML::script('js/ajax.js') }}
	{{ HTML::script('js/myscript.js') }}
	@show
</head>
<body>

	<div class="navbar">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="{{ URL::to('sections') }}" class="navbar-brand">Sourceeasy Documentation</a>
			</div>
			<div class="navbar-collapse collapse navbar-responsive-collapse">
				<ul class="nav navbar-nav">
					<li><a href="{{ URL::to('sections') }}">HOME</a></li>
					
				</ul>
				<ul class="nav navbar-nav navbar-right">
					
				</ul>
			</div>
		</div>
	</div>
	
	@yield('content')

	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</body>
</html>
<!-- 
<form method="POST" action="http://localhost/Sourceeasy-Docs/public/sections/Ea%20in%20consequatur%20optio." accept-charset="UTF-8"><input name="_method" type="hidden" value="PUT"><input name="_token" type="hidden" value="1QjdKCKBarvdxgCeHmrtpMD1B9TZmfK8DO9jzZ48">
				<input type="text" name="rename" id="rename" value="">
				<button type="submit">Submit</button>
			</form>
 -->
