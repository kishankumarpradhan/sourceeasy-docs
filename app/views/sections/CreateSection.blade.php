@extends('layouts.master')

@section('head')
	@parent

	<title>Sourceeasy Admin</title>

@stop
@section('content')
	
	@if(Session::has('success'))
		<div class="alert alert-success">{{ Session::get('success') }}</div>
	@elseif(Session::has('fail'))
		<div class="alert alert-danger">{{ Session::get('fail') }}</div>
	@endif

	<center><h2>Add new Section</h2></center>
	<div>
		<form method="post" action="{{ URL::to('sections') }}">
			<div class="form-group{{ ($errors->has('name')) ? ' has-error' : '' }}">
				<label for="name">Section Name</label>
				<input type="text" class="form-control" name="name" id="sname" placeholder="Enter section name">
				@if($errors->has('name'))
					{{ $errors->first('name') }}
				@endif
			</div>
			<div class="form-group">
				<button type="submit">Create Section</button>
			</div>
		</form>
	</div>

@stop