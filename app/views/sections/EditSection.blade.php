@extends('layouts.master')

@section('head')
	@parent

	<title>Sourceeasy Admin</title>

@stop
@section('content')
	
	@if(Session::has('success'))
		<div class="alert alert-success">{{ Session::get('success') }}</div>
	@elseif(Session::has('fail'))
		<div class="alert alert-danger">{{ Session::get('fail') }}</div>
	@endif

	<center><h2>{{ $section->name }}</h2></center>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
			{{ Form::model($questions, array('route' => array('sections.update', $section_slug), 'method' => 'PUT')) }}
				Rename : <input type="text" name="rename" id="rename" value="{{$section->name}}" >
				<button type="submit">Submit</button>
			{{ Form::close() }}
			</div>
			<div class="col-md-6" val="{{$section_slug}}" id="{{$section->id}}" check="do">
				<a href="javascript:void(0)" id="dsection">Delete this Section</a>
			</div>
		</div>
		<br><br>
		<div class="row">
			<div class="col-md-7">
				@if(count($questions) > 0)
					@foreach($questions as $question)
						<div id="quesdelete{{$question->id}}">
							<span>{{ $question->title }}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<span><a href="{{ URL::to('sections/'.$section_slug.'/questions/'.$question->slug.'/edit') }}">Edit</a></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<span id="{{$question->id}}"  qslug="{{$question->slug}}">
								<a href="javascript:void(0)" class="qdelete">Delete</a>
							</span>
							@if(Session::has('EQsuccess'.$question->id))
								<span class="alert alert-success">{{ Session::get('EQsuccess'.$question->id) }}</span>
							@elseif(Session::has('EQfail'.$question->id))
								<span class="alert alert-danger">{{ Session::get('EQfail'.$question->id) }}</span>
							@endif
						<hr>
						</div>
					@endforeach
				@else
					<div>No questions added to this sections</div>
				@endif
			</div>
			<div class="col-md-5">
				<h3>Add new Question</h3>
				<form method="post" action="{{ URL::to('sections/'.$section_slug.'/questions') }}">
					<div class="form-group{{ ($errors->has('title')) ? ' has-error' : '' }}">
						<label for="title">Question</label>
						<input type="text" class="form-control" name="title" id="title" placeholder="Type Question Here">
						@if($errors->has('title'))
							{{ $errors->first('title') }}
						@endif
					</div>
					<div class="form-group{{ ($errors->has('descriptions')) ? ' has-error' : '' }}">
						<label for="descriptions">Description</label>
						<textarea name="descriptions" id="descriptions" placeholder="Enter Description for Question Here"></textarea>
						@if($errors->has('descriptions'))
							{{ $errors->first('descriptions') }}
						@endif
					</div>
					<div class="form-group">
						<button type="submit">Create Question</button>
					</div>
				</form>
			</div>
		</div>
	</div>

@stop