@extends('layouts.master')

@section('head')
	@parent

	<title>Sourceeasy Docs</title>

@stop
@section('content')
@if(count($questions) > 0)
	@foreach($questions as $question)
		<div><a href="{{ URL::to('sections/'.$section_slugs.'/questions/'.$question->slug) }}">{{ $question->title }}</a><p>{{ $question->descriptions }}</p></div><hr>
	@endforeach
@else
	<div>Questions will be loaded soon.</div>
@endif
@stop