@extends('layouts.master')

@section('head')
	@parent

	<title>Sourceeasy Admin</title>

@stop
@section('content')
	
	@if(Session::has('success'))
		<div class="alert alert-success">{{ Session::get('success') }}</div>
	@elseif(Session::has('fail'))
		<div class="alert alert-danger">{{ Session::get('fail') }}</div>
	@endif

	<center><h2>Edit Question</h2></center>
	<div class="container">
		<div class="row">
			{{ Form::model($questions, array('route' => array('sections.questions.update', $section_slug, $questions->slug), 'method' => 'PUT')) }}
				<div class="form-group{{ ($errors->has('title')) ? ' has-error' : '' }}">
					<label for="title" >Title</label>
					<input type="text" name="title" id="title" value="{{$questions->title}}" >
					@if($errors->has('title'))
						{{ $errors->first('title') }}
					@endif
				</div>
				<div class="form-group{{ ($errors->has('descriptions')) ? ' has-error' : '' }}">
					<label for="descriptions">Description</label>
					<textarea name="descriptions" id="descriptions" placeholder="Enter Description for Question Here">{{ $questions->descriptions }}</textarea>
					@if($errors->has('descriptions'))
						{{ $errors->first('descriptions') }}
					@endif
				</div>
				<button type="submit">Submit</button>
			{{ Form::close() }}
		</div>
	</div>

@stop