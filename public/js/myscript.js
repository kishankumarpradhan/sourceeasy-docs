

$(document).ready(function(){
//-------For question Delete----------------------------------
	$('.qdelete').click(function(){
		//alert('ok');
        //var conf = confirm("Do you really want to delete");
		var qid = $(event.target).parent().attr('id');
		var slug = $("#dsection").parent().attr('val');
        var qslug = $(event.target).parent().attr('qslug');
        //alert(qid+' '+slug+' '+qslug);
		var poststr="qid="+qid;
		$.ajax({
                type: 'delete',
                url: 'http://localhost/Sourceeasy-Docs/public/sections/'+slug+'/questions/'+qslug,
                data: poststr,
                beforeSend: function() { 
                    //$("#aa").hide(); 
                },
                success: function(data) {
                    if(data.success == false)
                    {
                        alert("something wrong");
                    } 
                    else 
                    {
                        if(data == "success"){
                            $("#quesdelete"+qid).remove();
                        }
                        else{
                        	alert(data);
                        }
                    }
                },
                error: function(xhr, textStatus, thrownError) {
                    alert('Something went to wrong.Please Try again later...');
                }
         });
	});
//--------------------------------------------------------------------------------
//--------------For Section Delete -----------------------------------------------
    $('.sdelete, #dsection').click(function(){
        ///alert('ok');
        var conf = confirm("Do you really want to delete");
        if(conf == true){
            var sid = $(event.target).parent().attr('id');
            var slug = $(event.target).parent().attr('val');
            var check = $(event.target).parent().attr('check');
            //alert(sid+' '+slug);
            var poststr="sid="+sid;
            $.ajax({
                    type: 'delete',
                    url: 'http://localhost/Sourceeasy-Docs/public/sections/'+slug,
                    data: poststr,
                    beforeSend: function() { 
                        //$("#aa").hide(); 
                    },
                    success: function(data) {
                        if(data.success == false)
                        {
                            alert("something wrong");
                        } 
                        else 
                        {
                            if(data == "success"){
                                alert("Deleted");
                                
                                if(check == 'do')
                                {
                                    location.href="http://localhost/Sourceeasy-Docs/public/admin";
                                }
                                else
                                {
                                    $("#section"+sid).remove();
                                }
                            }
                            else{
                                alert(data);
                            }
                        }
                    },
                    error: function(xhr, textStatus, thrownError) {
                        alert('Something went to wrong.Please Try again later...');
                    }
             });
        }
    });
//--------------------------------------------------------------------------------
});
